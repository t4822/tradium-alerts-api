package com.tradiumapp.swingtradealerts.models;

public enum IndicatorType {
    price, rsi, sma, ema, week52High, week52Low, earningsDate, reddit
}
